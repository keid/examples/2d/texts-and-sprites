{-# LANGUAGE OverloadedLists #-}

module Stage.World.Flare
  ( Process
  , Config(..)
  , spawn

  , Observer
  ) where

import RIO

import Control.Monad.Trans.Resource (MonadResource)
import Data.Vector qualified as Vector
import Data.Vector.Storable qualified as Storable
import Geomancy (vec4)
import Geomancy.Transform qualified as Transform
import Vulkan.Zero (zero)
import System.Random (newStdGen, random)
import System.Random.Stateful (applySTGen, newSTGenM)

import Engine.Worker qualified as Worker
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Model qualified as UnlitTextured

type Process = Worker.Timed Config UnlitTextured.Stores

data Config = Config
  { textureId  :: Int32
  , originX    :: Float
  , originY    :: Float
  , turnBase   :: Float
  , turnSpread :: Float
  , spawnRate  :: Either Float Int
  , spawnSide  :: Either () ()
  }

data Flare = Flare
  { fuse      :: Float
  , energy    :: Float
  , direction :: Float
  , x         :: Float
  , y         :: Float
  }

spawn
  :: ( MonadResource m
     , MonadUnliftIO m
     )
  => Config
  -> m Process
spawn = Worker.spawnTimed True (Left dt) initF stepF
  where
    initF _config =
      pure
        ( UnlitTextured.Attrs mempty mempty
        , mempty
        )

dt :: Num a => a
dt = 15_625

stepF
  :: MonadIO m
  => Vector Flare
  -> Config
  -> m (Maybe UnlitTextured.Stores, Vector Flare)
stepF current Config{..} = do
  seed <- newStdGen
  nextFlares <- evaluate $ runST do
    gen <- newSTGenM seed
    Vector.mapMaybeM (advanceEx gen) current

  seedNew <- newStdGen
  new <- evaluate $ runST do
    gen <- newSTGenM seedNew
    extra <- case spawnRate of
      Left scale -> do
        uniform <- applySTGen random gen
        pure . round $ uniform * scale
      Right limit ->
        pure . max 0 $ limit - Vector.length current
    Vector.replicateM extra do
      !fuse' <- applySTGen random gen
      !τ' <- applySTGen random gen
      pure $! spawnFlare fuse' τ'

  pure
    ( Just $ nextBufs nextFlares
    , case spawnSide of
        Left () ->
          new <> nextFlares
        Right () ->
          nextFlares <> new
    )
  where
    advanceEx gen flare = do
      !driftDir <- applySTGen random gen
      pure $! advance driftDir flare

    advance driftD Flare{..} =
      if energy > 0 then
        Just Flare
          { fuse      = fuse'
          , energy    = energy'
          , direction = direction'
          , x         = x'
          , y         = y'
          }
      else
        Nothing
      where
        fuse'  = max 0 $ fuse - dt
        energy' = energy - dt + fuse'
        direction' = direction + (driftD - 0.5) / 12
        x'     = x + cos direction'
        y'     = y + sin direction'

    maxEnergy = 20_000_000

    spawnFlare fuse' τ' = Flare
      { fuse      = dt * 45 * fuse'
      , energy    = 1_000_000
      , direction = τ * (turnBase + turnSpread * (τ' - 0.5))
      , x         = originX
      , y         = originY
      }

    nextBufs nextFlares = UnlitTextured.Attrs{params, transforms}
      where
        params = Storable.convert do
          Flare{..} <- nextFlares
          pure $! zero
            { UnlitTextured.tpSamplerId = Samplers.linearMip Samplers.indices
            , UnlitTextured.tpTextureId = textureId
            , UnlitTextured.tpGamma = vec4 1 1 1 (min (maxEnergy / 2) energy / maxEnergy)
            }

        transforms = Storable.convert do
          Flare{..} <- nextFlares
          pure $! mconcat
            [ Transform.scale 32
            , Transform.translate x y 0
            ]

type Observer = Worker.ObserverIO UnlitTextured.Buffers

τ :: Float
τ = 2 * pi
