{-# LANGUAGE OverloadedLists #-}

module Stage.World.Stars
  ( Process
  , Config(..)
  , Layer(..)
  , spawn

  , Observer
  ) where

import RIO

import Control.Monad.Trans.Resource (MonadResource)
import Data.Vector qualified as Vector
import Data.Vector.Storable qualified as Storable
import Geomancy (Vec2, vec2)
import Geomancy.Transform qualified as Transform
import Vulkan.Zero (zero)

import Engine.Worker qualified as Worker
import Render.Samplers qualified as Samplers
import Render.Unlit.Textured.Model qualified as UnlitTextured

type Process = Worker.Timed Config UnlitTextured.Stores

data Config = Config
  { textureId :: Int32
  }

data Layer = Layer
  { direction :: Float
  , offset    :: Vec2
  , scale     :: Vec2
  }

spawn
  :: ( MonadResource m
     , MonadUnliftIO m
     )
  => Vector Layer
  -> Config
  -> m Process
spawn layers =
  Worker.spawnTimed True (Left dt) initF \flares ->
    pure . stepF flares
  where
    initF _config =
      pure
        ( UnlitTextured.Attrs mempty mempty
        , layers
        )

dt :: Num a => a
dt = 15_625

stepF :: Vector Layer -> Config -> (Maybe UnlitTextured.Stores, Vector Layer)
stepF layers Config{textureId} =
  let
    nextStars = fmap advance layers
  in
    ( Just $ nextBufs nextStars
    , nextStars
    )
  where
    advance old@Layer{direction, offset} = old
      { offset = offset + vec2 (cos direction) (sin direction) / 4096.0
      }

    nextBufs nextStars = UnlitTextured.Attrs{params, transforms}
      where
        params = Storable.convert do
          Layer{..} <- nextStars
          pure $! zero
            { UnlitTextured.tpSamplerId = Samplers.linearRepeat Samplers.indices
            , UnlitTextured.tpTextureId = textureId
            , UnlitTextured.tpOffset    = offset
            , UnlitTextured.tpScale     = scale
            }

    transforms =
      Storable.replicate (Vector.length layers) (Transform.scale 4096)

type Observer = Worker.ObserverIO UnlitTextured.Buffers
