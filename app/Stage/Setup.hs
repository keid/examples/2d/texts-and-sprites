module Stage.Setup
  ( stackStage
  , Stage
  , stage
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.List qualified as List
import Geomancy (vec2, vec4)
import Geomancy.Layout.Alignment qualified as Alignment
import Geomancy.Layout.Box (box_)
import Geomancy.Layout.Box qualified as Box
import GHC.RTS.Flags (getRTSFlags)
import RIO.State (gets)
import RIO.Vector qualified as Vector
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk
-- import RIO.Process (proc, runProcess)

import Engine.Camera qualified as Camera
import Engine.StageSwitch (trySwitchStage)
import Engine.Types qualified as Engine
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (HasSwapchain, Queues)
import Engine.Worker qualified as Worker
import Geomancy.Layout qualified as Layout
import Geometry.Quad qualified as Quad
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Samplers qualified as Samplers
import Resource.Collection qualified as Collection
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer qualified as CommandBuffer
import Resource.Font qualified as Font
import Resource.Model qualified as Model
import Resource.Model.Observer qualified as Observer
import Resource.Region qualified as Region
import Resource.Texture.Ktx2 qualified as Ktx2

import Stage.Render qualified as Render
import Stage.Resource.Font qualified as StageFont
import Stage.Resource.Texture.Combined qualified as Combined
import Stage.Resource.Texture.Flat qualified as Flat
import Stage.Types (FrameResources(..), RunState(..), Stage)
import Stage.World qualified as World
import Stage.World.Flare qualified as Flare
import Stage.World.Stars qualified as Stars

stackStage :: Engine.StackStage
stackStage = Engine.StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sAllocateRP = Basic.allocate_
  , sAllocateP  = allocatePipelines
  , sInitialRS  = initialRunState
  , sInitialRR  = intialRecyclableResources
  , sBeforeLoop = async World.animateMessages

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop    = cancel
  }

allocatePipelines
  :: HasSwapchain swapchain
  => swapchain
  -> Basic.RenderPasses
  -> ResourceT (Engine.StageRIO RunState) Basic.Pipelines
allocatePipelines swapchain renderpasses = do
  -- Allocate static set of common samplers.
  samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)

  -- Configure descriptors for statically-known set of flat textures, no cubes, and no shadows.
  let sceneBinds = Scene.mkBindings samplers Combined.sources Nothing 0

  Basic.allocatePipelines sceneBinds (Swapchain.getMultisample swapchain) renderpasses

initialRunState :: Engine.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState = CommandBuffer.withPools \pools -> Region.run do
  rts <- liftIO getRTSFlags
  logDebug $ displayShow rts

  ortho <- Camera.spawnOrthoPixelsCentered
  rsSceneP <- Worker.spawnMerge1 mkScene ortho

  logInfo "Loading models"
  rsQuadUV <- Model.createStagedL (Just "rsQuadUV") pools (Quad.toVertices Quad.texturedQuad) Nothing
  Model.registerIndexed_ rsQuadUV

  logInfo "Loading fonts"
  fonts <- traverse (Font.allocate pools) StageFont.configs

  logInfo "Loading textures"
  textures <- traverse (Ktx2.load pools) Flat.sources

  let
    rsFonts = fmap Font.container fonts

    rsTextures = CombinedTextures.Collection
      { textures = textures
      , fonts    = fmap Font.texture fonts
      }
    textureIds = fmap fst $ Collection.enumerate rsTextures

  CombinedTextures.attachDebugNames
    rsTextures
    (fmap show Flat.sources)
    (fmap (show . Font.configTexture) StageFont.configs)

  rsStarsP <-
    Stars.spawn World.starLayers
      ( Stars.Config
          { textureId = Flat.stars $ CombinedTextures.textures textureIds
          }
      )

  rsFlaresP <-
    Flare.spawn Flare.Config
      { textureId  = Flat.cd $ CombinedTextures.textures textureIds
      , spawnRate  = Right $ 2 ^ (14 :: Int)
      , spawnSide  = Right ()
      , originX    = 0
      , originY    = 600
      , turnBase   = -1/4
      , turnSpread = 1/3
      }

  rsFlares1P <-
    Flare.spawn Flare.Config
      { textureId  = Flat.david $ CombinedTextures.textures textureIds
      , spawnRate  = Left 0 -- XXX: on first kick
      , spawnSide  = Left ()
      , originX    = -750
      , originY    = 450
      , turnBase   = -1/4 + 1/8
      , turnSpread = 1/6
      }

  rsFlares2P <-
    Flare.spawn Flare.Config
      { textureId  = Flat.fashion $ CombinedTextures.textures textureIds
      , spawnRate  = Left 0 -- XXX: on first kick
      , spawnSide  = Left ()
      , originX    = 750
      , originY    = 450
      , turnBase   = -1/4 - 1/8
      , turnSpread = 1/6
      }

  rsFlares3P <-
    Flare.spawn Flare.Config
      { textureId  = Flat.flare $ CombinedTextures.textures textureIds
      , spawnRate  = Left 0 -- XXX: on second kick
      , spawnSide  = Left ()
      , originX    = 0
      , originY    = 750
      , turnBase   = -1/4
      , turnSpread = 1/6
      }

  void $! async do
    threadDelay 2e6
    logInfo "Pre kick"

    Worker.modifyConfig rsFlares1P \config -> config
      { Flare.spawnRate = Right 1
      }

    Worker.modifyConfig rsFlares2P \config -> config
      { Flare.spawnRate = Right 1
      }

  void $! async do
    threadDelay 6e6
    logInfo "First kick"

    Worker.modifyConfig rsFlares1P \config -> config
      { Flare.spawnRate = Left 8
      }

    Worker.modifyConfig rsFlares2P \config -> config
      { Flare.spawnRate = Left 8
      }

  void $! async do
    threadDelay 8.5e6 -- Time offset to raise inot screen
    logInfo "Second kick"
    Worker.modifyConfig rsFlares3P \config -> config
      { Flare.spawnRate = Left 100
      }

  void $! async do
    threadDelay 45e6
    logInfo "Fading out..."
    for_ [100, 99 .. 0] \(_volume :: Int) -> do
      threadDelay 100e3
      for_ [rsFlaresP, rsFlares1P, rsFlares2P, rsFlares3P] \flare ->
        Worker.modifyConfig flare \config@Flare.Config{spawnRate} -> config
          { Flare.spawnRate =
              bimap
                (\rate -> rate * 0.9)
                (const 0)
                spawnRate
          }

  void $! async do
    threadDelay 57e6
    -- _done <- proc
    --   "mplayer"
    --   [ "idol.opus"
    --   , "-quiet"
    --   -- , "-slave"
    --   -- , "-input", "file=" <> fifo
    --   -- , "-volume", "0"
    --   , "-ss"
    --   , "195" -- "90"
    --   ]
    --   runProcess

    threadDelay 15e6

    -- TODO: when benchmark
    -- threadDelay 60e6

    Worker.Versioned{vVersion} <- readTVarIO (Worker.tOutput rsFlaresP)
    logInfo $ "Flare updates: " <> displayShow vVersion

    lift $ trySwitchStage Engine.Finish

  -- And now for something completely different....

  let
    fontLarge = StageFont.large rsFonts
    fontLargeId = StageFont.large (CombinedTextures.fonts textureIds)

  screenBoxP <- Engine.askScreenVar >>=
    Worker.spawnMerge1 \Vk.Extent2D{width, height} ->
      box_ $ vec2 (fromIntegral width) (fromIntegral height)

  screenPaddedP <- Worker.spawnMerge1 (Box.addPadding $ Box.TRBL 32) screenBoxP

  let
    origins =
      [ Alignment.leftTop
      , Alignment.leftMiddle
      , Alignment.leftBottom
      , Alignment.centerTop
      , Alignment.center
      , Alignment.centerBottom
      , Alignment.rightTop
      , Alignment.rightMiddle
      , Alignment.rightBottom
      ]
  originInputs <- for origins \inputOrigin -> do
    let
      inputText         = ""
      inputFont         = fontLarge
      inputFontId       = fontLargeId
      inputSize         = 0 -- XXX: Animated
      inputColor        = 0
      inputOutline      = 0
      inputOutlineWidth = 4/16
      inputSmoothing    = 1/16

    inputVar <- Worker.newVar Message.Input{..}
    pure (screenPaddedP, inputVar)

  messageGroup <- traverse (uncurry Message.spawn) originInputs
  let rsMessageOrigins = List.zip (map snd originInputs) messageGroup

  splitVar <- Worker.newVar 0.5
  async $ forever do
    threadDelay 10_000
    t <- fmap realToFrac getMonotonicTime
    Worker.pushInput splitVar $ const (sin (t * 0.125) * 0.25 + 0.5)

  screenHalvesP <- Worker.spawnMerge2 Layout.splitLeft splitVar screenBoxP

  leftHalfP <- Worker.spawnMerge1 fst screenHalvesP
  leftInput <- Worker.newVar Message.Input
    { inputText         = "‹ left text ‹"
    , inputFontId       = fontLargeId
    , inputFont         = fontLarge
    , inputOrigin       = Alignment.rightMiddle
    , inputSize         = 64
    , inputColor        = 1
    , inputOutline      = vec4 0 0 0 1
    , inputOutlineWidth = 4/16
    , inputSmoothing    = 1/16
    }
  leftMessageP <- Message.spawn leftHalfP leftInput

  rightHalfP <- Worker.spawnMerge1 snd screenHalvesP
  rightInput <- Worker.newVar Message.Input
    { inputText         = "› right text ›"
    , inputFontId       = fontLargeId
    , inputFont         = fontLarge
    , inputOrigin       = Alignment.leftMiddle
    , inputSize         = 64
    , inputColor        = 1
    , inputOutline      = vec4 0 0 0 1
    , inputOutlineWidth = 4/16
    , inputSmoothing    = 1/16
    }
  rightMessageP <- Message.spawn rightHalfP rightInput
  let rsMessageSplit = [leftMessageP, rightMessageP]

  pure RunState{..}
  where
    mkScene Camera.Projection{..} = Scene.emptyScene
      { Scene.sceneProjection = projectionTransform
      }

intialRecyclableResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools _renderPasses pipelines = do
  combinedTextures <- gets rsTextures
  frScene <- Scene.allocate
    (Basic.getSceneLayout pipelines)
    combinedTextures
    []
    Nothing
    mempty
    Nothing

  frStars <- Observer.newCoherent (Vector.length World.starLayers) "Stars"

  frFlares  <- Observer.newCoherent 32768 "flares0"
  frFlares1 <- Observer.newCoherent 32768 "flares1"
  frFlares2 <- Observer.newCoherent 32768 "flares2"
  frFlares3 <- Observer.newCoherent 32768 "flares3"

  origins <- gets rsMessageOrigins
  frMessageOrigins <- for origins \_worker ->
    Message.newObserver 80

  split <- gets rsMessageSplit
  frMessageSplit <- for split \_worker ->
    Message.newObserver 80

  pure FrameResources{..}
