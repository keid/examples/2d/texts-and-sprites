{-# LANGUAGE DeriveAnyClass #-}

module Stage.Resource.Font
  ( Collection(..)
  , ConfigCollection
  , FontCollection
  , TextureCollection
  , Font.Config(..)
  , configs
  ) where

import RIO

import GHC.Generics (Generic1)

import Resource.Collection (Generically1(..))
import Resource.Font qualified as Font
import Resource.Font.EvanW qualified as EvanW
import Resource.Source (Source(..))
import Resource.Static as Static
import Resource.Texture (Texture, Flat)

type ConfigCollection = Collection Font.Config
type FontCollection = Collection EvanW.Container
type TextureCollection = Collection (Texture Flat)

data Collection a = Collection
  { small :: a
  , large :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Generically1 Collection)

Static.filePatterns Static.Files "resources/fonts/evanw-sdf"

configs :: Collection Font.Config
configs = Collection
  { small = Font.Config
      { Font.configContainer = File Nothing UBUNTU_32_5_JSON
      , Font.configTexture   = File Nothing UBUNTU_32_5_KTX2
      }
  , large = Font.Config
      { Font.configContainer = File Nothing UBUNTU_256_8_JSON
      , Font.configTexture   = File Nothing UBUNTU_256_8_KTX2
      }
  }
