{-# LANGUAGE DeriveAnyClass #-}

{-# OPTIONS_GHC -Wno-unused-top-binds #-}
-- XXX: the example uses shared textured and only needs some of them

module Stage.Resource.Texture.Flat
  ( Collection(..)
  , TextureCollection
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import RIO.FilePath ((</>))

import Global.Resource.Texture.Base qualified as Base
import Resource.Collection (Generically1(..))
import Resource.Source (Source(..))
import Resource.Static as Static
import Resource.Texture (Texture, Flat)

data Collection a = Collection
  { base    :: Base.Collection a
  , cd      :: a
  , david   :: a
  , fashion :: a
  , flare   :: a
  , stars   :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type TextureCollection = Collection (Int32, Texture Flat)

Static.filePatterns Static.Files ("resources" </> "textures")

sources :: Collection Source
sources = Collection
  { base    = Base.sources
  , cd      = File Nothing CD_KTX2
  , david   = File Nothing DAVID_KTX2
  , fashion = File Nothing FASHION_KTX2
  , flare   = File Nothing FLARE_KTX2
  , stars   = File Nothing STARS_KTX2
  }
