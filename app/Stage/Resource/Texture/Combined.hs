module Stage.Resource.Texture.Combined where

import RIO

import Resource.Collection (enumerate, size)
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.Texture (Texture, Flat)
import Resource.Source (Source)

import Stage.Resource.Font qualified as Font
import Stage.Resource.Texture.Flat qualified as Flat

type TextureCollectionF a =
  CombinedTextures.Collection
    Flat.Collection
    Font.Collection
    a

type CombinedCollection = TextureCollectionF (Int32, Texture Flat)
type Ids                = TextureCollectionF Int32
type Textures           = TextureCollectionF (Texture Flat)

sources :: TextureCollectionF Source
sources = CombinedTextures.Collection
  { textures = Flat.sources
  , fonts    = fmap Font.configTexture Font.configs
  }

indices :: TextureCollectionF Int32
indices = fmap fst $ enumerate sources

numTextures :: Num a => a
numTextures = size sources
