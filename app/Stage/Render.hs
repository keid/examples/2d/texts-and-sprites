{-# LANGUAGE OverloadedRecordDot #-}

module Stage.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import RIO.State (gets)
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types qualified as Engine
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Resource.Model.Observer qualified as Observer

import Stage.Types (FrameResources(..), RunState(..))

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Scene.observe rsSceneP frScene

  Observer.observeCoherent rsStarsP frStars
  Observer.observeCoherent rsFlaresP frFlares
  Observer.observeCoherent rsFlares1P frFlares1
  Observer.observeCoherent rsFlares2P frFlares2
  Observer.observeCoherent rsFlares3P frFlares3

  for_ (zip rsMessageOrigins frMessageOrigins) \((_input, process), buf) ->
    Message.observe process buf

  for_ (zip rsMessageSplit frMessageSplit) $
    uncurry Message.observe

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> "image index" ::: Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  frame@Engine.Frame{fPipelines} <- asks snd

  ForwardMsaa.usePass frame.fRenderpass.rpForwardMsaa imageIndex cb do
    Swapchain.setDynamicFullscreen cb frame

    Scene.withBoundSet0 frScene fPipelines.pUnlitTexturedBlend cb do
      Graphics.bind cb fPipelines.pUnlitTexturedBlend do
        texturedQuad <- gets rsQuadUV

        traverse_
          (Worker.readObservedIO >=> Draw.indexed cb texturedQuad)
          [ frStars
          , frFlares
          , frFlares1
          , frFlares2
          , frFlares3
          ]

      Graphics.bind cb fPipelines.pEvanwSdf $
        for_ (frMessageOrigins <> frMessageSplit) $
          Worker.readObservedIO >=> Draw.quads cb
