module Stage.Types
  ( Stage
  , FrameResources(..)
  , RunState(..)
  ) where

import Engine.UI.Message qualified as Message
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Resource.Buffer qualified as Buffer

import Stage.Resource.Font (FontCollection)
import Stage.Resource.Texture.Combined qualified as Combined
import Stage.World.Flare qualified as Flare
import Stage.World.Stars qualified as Stars

type Stage = Basic.Stage FrameResources RunState

data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]

  , frStars :: Stars.Observer

  , frFlares  :: Flare.Observer
  , frFlares1 :: Flare.Observer
  , frFlares2 :: Flare.Observer
  , frFlares3 :: Flare.Observer

  , frMessageSplit   :: [Message.Observer]
  , frMessageOrigins :: [Message.Observer]
  }

data RunState = RunState
  { rsSceneP      :: Set0.Process
  , rsQuadUV      :: UnlitTextured.Model 'Buffer.Staged
  , rsFonts       :: FontCollection
  , rsTextures    :: Combined.Textures

  , rsStarsP :: Stars.Process

  , rsFlaresP  :: Flare.Process
  , rsFlares1P :: Flare.Process
  , rsFlares2P :: Flare.Process
  , rsFlares3P :: Flare.Process

  , rsMessageOrigins :: [(Worker.Var Message.Input, Message.Process)]
  , rsMessageSplit   :: [Message.Process]
  }
